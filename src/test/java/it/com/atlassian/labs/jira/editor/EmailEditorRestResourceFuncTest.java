package it.com.atlassian.labs.jira.editor;

import com.atlassian.labs.jira.emaileditor.rest.EmailEditorRestResourceModel;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EmailEditorRestResourceFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/emaileditorrest/1.0/message";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        EmailEditorRestResourceModel message = resource.get(EmailEditorRestResourceModel.class);

    }
}
