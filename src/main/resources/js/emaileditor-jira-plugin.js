(function($){
    $(document).ready(function(){
        $('#codediv').hide();
        $('.aui-icon').hide();
        $('#restartmessage').hide();
        var editor = CodeMirror.fromTextArea(document.getElementById("codetextarea"), {
            tabMode: "indent",
            theme: "neat",
            lineNumbers: true,
            lineWrapping: true,
            indentUnit: 4,
            autoCloseBrackets: true,
            mode: "text/velocity"
        });

        $('.templatefile').click(function(){
            event.preventDefault();
            $('#selectedfile').val($(this).text());
            $('#selectedtype').val(AJS.$(this).attr('type'));

            $('#helpmessage').hide();
            $('#restartmessage').hide();
            $('.aui-icon').hide();
            $('#savemessage').text('');
            $('.templatefile').parent().removeClass('aui-nav-selected');
            $(this).parent().addClass('aui-nav-selected');
            $('#templateheading').text("Editing " + $(this).text());
            $.getJSON(AJS.contextPath() + '/rest/emaileditor/latest/template/getcontents?name=' + $(this).text() + '&type=' +
                AJS.$(this).attr('type'), function(data){
                $('#codediv').show();
                editor.setValue(data.contents);
                editor.focus();
            });
        });
        $('#templatesave').click(function(){
            event.preventDefault();
            $('#waiticon').show();
            $('#savemessage').text(' Saving data');
            $('#templatesave').attr('disabled', 'disabled');
            var postdata = {};
            postdata.name = $('#selectedfile').val();
            postdata.type = $('#selectedtype').val();
            postdata.contents = editor.getValue();
            $.ajax({
                url: AJS.contextPath() + "/rest/emaileditor/latest/template/updatecontents",
                data: JSON.stringify(postdata),
                type: "POST",
                contentType: "application/json",
                success: function (data) {
                    //console.log("New content update added :" + data);
                    $('#savemessage').text(' Last saved at ' + new Date().toLocaleTimeString());
                    $('.aui-icon-error').hide();
                    $('.aui-icon-success').show();
                    $('#restartmessage').show();
                },
                error: function(data){
                    console.log("Failed to update contents");
                    $('#savemessage').text(' Failed to save the updated data');
                    $('.aui-icon-error').show();
                    $('.aui-icon-success').hide();
                },
                complete: function(data) {
                    //console.log("Update completed");
                    $('#templatesave').removeAttr('disabled');
                    $('#waiticon').hide();
                }
            });
        });
    });
})(AJS.$);