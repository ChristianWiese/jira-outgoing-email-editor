package com.atlassian.labs.jira.emaileditor.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import webwork.util.ClassLoaderUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;

/**
 * A resource of message.
 */
@Path("/template")
@WebSudoRequired
public class EmailEditorRestResource {

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("getcontents")
    @RequiresXsrfCheck
    public Response getContents(@QueryParam("name") String filename, @QueryParam("type") String type) {
        if (filename == null || type == null || filename.isEmpty() || type.isEmpty()) {
            return Response.notModified("Required parameters missing (name and type").build();
        }

        if( !ComponentAccessor.getPermissionManager().hasPermission(Permissions.SYSTEM_ADMIN,
                ComponentAccessor.getJiraAuthenticationContext().getUser()) )
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        final EmailEditorRestResourceModel emailEditorRestResourceModel = new EmailEditorRestResourceModel();
        emailEditorRestResourceModel.setType(type);
        emailEditorRestResourceModel.setFilename(filename);

        try {
            String file = ClassLoaderUtils.getResource(String.format("templates/email/%s/%s",
                    emailEditorRestResourceModel.getType(), emailEditorRestResourceModel.getFilename()),
                    this.getClass()).getFile();
            emailEditorRestResourceModel.setFilename(file);
            FileReader reader = new FileReader(file);
            BufferedReader buffer = new BufferedReader(reader);
            String completeFile = "";
            String fileContent;
            while ((fileContent = buffer.readLine()) != null) {
                completeFile += fileContent + "\n";
            }
            buffer.close();
            reader.close();
            emailEditorRestResourceModel.setContents(completeFile);
        } catch (FileNotFoundException e) {
            emailEditorRestResourceModel.setContents(e.getMessage());
        } catch (IOException e) {
            emailEditorRestResourceModel.setContents(e.getMessage());
        }

        return Response.ok(emailEditorRestResourceModel).build();
    }

    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("updatecontents")
    @RequiresXsrfCheck
    public Response updateContents(final EmailEditorRestResourceModel templateData) {
        if (templateData == null ||
                templateData.getFilename() == null || templateData.getFilename().isEmpty() ||
                templateData.getType() == null || templateData.getType().isEmpty() ||
                templateData.getContents() == null || templateData.getContents().isEmpty()) {
            return Response.notModified("Required parameters missing (name, content and type").build();
        }
        if( !ComponentAccessor.getPermissionManager().hasPermission(Permissions.SYSTEM_ADMIN,
                ComponentAccessor.getJiraAuthenticationContext().getUser()) )
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        try {
            String file = ClassLoaderUtils.getResource(String.format("templates/email/%s/%s",templateData.getType(), templateData.getFilename()),
                    this.getClass()).getFile();
            templateData.setFilename(file);
            FileWriter writer = new FileWriter(file);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(templateData.getContents());
            buffer.close();
            writer.close();
        } catch (FileNotFoundException e) {
            templateData.setContents(e.getMessage());
        } catch (IOException e) {
            templateData.setContents(e.getMessage());
        }
        return Response.ok(templateData).build();
    }
}