This plugin satisfies the ever pending feature request for JIRA (https://jira.atlassian.com/browse/JRA-7266) for an web based editor for the email templates for JIRA outgoing emails.

## Features

* Supports editing of HTML and Text templates for outgoing emails
* Support code highlighting (based on http://codemirror.net/)
* Live Preview (coming soon)